# VS Code extensions and settings

 - Upon cloning, move extensions folder in your `.vscode` folder in home page.
 - And move `settings.json` to `code/Users` folder in `.config` folder.
 - Note that the name and location of these folders might differ from system to system.
