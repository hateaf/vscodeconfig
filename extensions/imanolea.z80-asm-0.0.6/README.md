## Z80 Assembly for Visual Studio Code

The Z80 Assembly extension for Visual Studio provides the following features inside VS Code:

* Syntax Highlighting for Z80 Assembly

### License
The Z80 Assembly extension is subject to [these license terms](https://github.com/Imanolea/z80asm-vscode/blob/master/LICENSE).  
The source code to this extension is available on [https://github.com/Imanolea/z80asm-vscode](https://github.com/Imanolea/z80asm-vscode) and licensed under the [MIT license](https://github.com/Imanolea/z80asm-vscode/blob/master/LICENSE).  

